package br.com.toledo.conexao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import br.com.toledo.dao.ProdutoDAO;
import br.com.toledo.dto.ProdutoDTO;

public class ConexaoProduto {

	private Context mContext;
	private ProdutoDAO mProdutoDAO;

	public List<ProdutoDTO> buscarTodosProdutos(Context context) {
		mContext = context;
		mProdutoDAO = ProdutoDAO.getInstance(mContext);

		SoapObject request = new SoapObject(ConexaoUtil.NAMESPACE, ConexaoUtil.BUSCAR_TODOS_PRODUTOS_METHOD_NAME);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);

		HttpTransportSE http = new HttpTransportSE(ConexaoUtil.URL, ConexaoUtil.TIMEOUT);
		http.debug = true;

		List<ProdutoDTO> retorno = null;

		try {
			http.call("", envelope);
			SoapObject responseObject = (SoapObject) envelope.bodyIn;

			retorno = new ArrayList<ProdutoDTO>();
			SoapObject element = null;

			for (int i = 0; i < responseObject.getPropertyCount(); i++) {
				element = (SoapObject) responseObject.getProperty(i);

				int id = element.getProperty("id") == null ? -1 : Integer.parseInt(element.getPropertyAsString("id"));
				String descricao = element.getProperty("descricao") == null ? "": element.getPropertyAsString("descricao");
				Double valor = element.getProperty("valor") == null ? 0f : Double.parseDouble(element.getPropertyAsString("valor"));
				String imagem = element.getProperty("imagem") == null ? "" : element.getPropertyAsString("imagem");
				String situacao = element.getProperty("situacao") == null ? "" : element.getPropertyAsString("situacao");
				
				String byteImage = element.getProperty("byteImage") == null ? "" : element.getPropertyAsString("byteImage");

				if (id > 0) {
					ProdutoDTO produto = mProdutoDAO.buscarProdutoPorId(id);
					if (produto == null) {
						produto = new ProdutoDTO(id, descricao, valor, imagem, situacao);
					} else {
						produto.setDescricao(descricao);
						produto.setValor(valor);
						produto.setImage(imagem);
						produto.setSituacao(situacao);
						saveImage(produto, byteImage);
					}

					mProdutoDAO.manter(produto);
					retorno.add(produto);
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}

		mProdutoDAO = null;
		return retorno;
	}

	public void saveImage(ProdutoDTO produto, String imageString) {
		new File(Environment.getExternalStorageDirectory() + "/projeto").mkdirs();
		
		FileOutputStream fileOutput = null;
		try {
			String filePath = ConexaoUtil.PATH_IMAGE + String.valueOf(produto.getId() + ".jpg");

			fileOutput = new FileOutputStream(filePath);

			byte[] bloc = Base64.decode(imageString, Base64.DEFAULT);         
			Bitmap bmp = BitmapFactory.decodeByteArray(bloc, 0, bloc.length);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, fileOutput);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fileOutput != null)
					fileOutput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

