package br.com.toledo.conexao;

import android.os.Environment;

public class ConexaoUtil {

	public static final String URL = "http://192.168.0.164:8080/WsTcc/services/UsuarioDAO?wsdl";	
	public static final String NAMESPACE ="http://webservice";
	
	public static final int TIMEOUT = (4 * 1000);
	
	public static final String LOGIN_METHOD_NAME = "buscarUsuarioPorNomeSenha";
	public static final String BUSCAR_TODOS_PRODUTOS_METHOD_NAME = "buscarTodosProdutos";
	

	public static final String PATH_IMAGE = Environment.getExternalStorageDirectory() + "/projeto/";

}
