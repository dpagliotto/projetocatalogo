package br.com.toledo.conexao;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.content.SharedPreferences;
import br.com.toledo.dto.UsuarioDTO;

public class ConexaoLogin {
	
	private static String SHARED_PREFERENCES_FILE = "Projeto";

	public UsuarioDTO buscarUsuarioPorNomeSenha(Context context, 
			String nome, String senha) {
		SoapObject request = new SoapObject(ConexaoUtil.NAMESPACE, ConexaoUtil.LOGIN_METHOD_NAME);
		
		PropertyInfo propertyNome = new PropertyInfo();
		propertyNome.setName("nome");
		propertyNome.setValue(nome);
		request.addProperty(propertyNome);
		
		PropertyInfo propertySenha = new PropertyInfo();
		propertySenha.setName("senha");
		propertySenha.setValue(senha);
		request.addProperty(propertySenha);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE http = new HttpTransportSE(ConexaoUtil.URL, ConexaoUtil.TIMEOUT);
		http.debug = true;
		
		SoapObject resp = null;
		UsuarioDTO retorno = null;
		
		try {
			http.call("", envelope);
			resp = (SoapObject) envelope.getResponse();

			String nomeUsuario = resp.getPropertyAsString("nome");
			String senhaUsuario = resp.getPropertyAsString("senha");
			Double comissaoUsuario = Double.valueOf(resp.getPropertyAsString("comissao"));
			
			retorno = new UsuarioDTO();
			retorno.setNome(nomeUsuario);
			retorno.setSenha(senhaUsuario);
			retorno.setComissao(comissaoUsuario);
			
			atualizaUsuarioPreferencias(context, retorno);
		} catch (Exception e) {
			retorno = recuperaUsuarioPreferencias(context);
		}
		
		return retorno;
	}

	private void atualizaUsuarioPreferencias(Context context, UsuarioDTO usuario) {
		SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
		
		preferences.edit().putString("nome", usuario.getNome()).commit();
		preferences.edit().putString("senha", usuario.getSenha()).commit();
		preferences.edit().putFloat("comissao", usuario.getComissao().floatValue()).commit();
	}

	private UsuarioDTO recuperaUsuarioPreferencias(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
		double comissao = preferences.getFloat("comissao", (float) 0.0);

		UsuarioDTO retorno = new UsuarioDTO();
		retorno .setNome(preferences.getString("nome", ""));
		retorno.setSenha(preferences.getString("senha", ""));
		retorno.setComissao(comissao);
		
		return retorno;
	}

}

