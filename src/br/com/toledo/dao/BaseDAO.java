package br.com.toledo.dao;

import android.database.sqlite.SQLiteDatabase;

public class BaseDAO {
	private static SQLiteDatabase dataBase;

	public static void setDataBase(SQLiteDatabase dataBase) {
		if(BaseDAO.dataBase == null || !BaseDAO.dataBase.isOpen())
		{
			BaseDAO.dataBase = dataBase;
		}
	}

	public static SQLiteDatabase getDataBase() {
		return dataBase;
	}
}

