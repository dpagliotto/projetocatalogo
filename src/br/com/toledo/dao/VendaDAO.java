package br.com.toledo.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.toledo.dto.ClienteDTO;
import br.com.toledo.dto.ComissaoDTO;
import br.com.toledo.dto.ProdutoDTO;
import br.com.toledo.dto.VendaDTO;
import br.com.toledo.dto.VendaItemDTO;

public class VendaDAO {
	
	public SQLiteDatabase db;
	public static VendaDAO instance = new VendaDAO();
	private Context context;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
	
	public static VendaDAO getInstance(Context contexto) {
		if (instance.db == null || instance.db.isOpen()) {
			instance.db = new BD(contexto).getWritableDatabase();
			instance.context = contexto;
		}

		return instance;
	}
	
	public List<VendaDTO> listarVendas() {
		List<VendaDTO> retorno = new ArrayList<VendaDTO>();
		
		Cursor cursor = db.query(BD.TBL_VENDA, null, null, null, null, null, null);
		while (cursor.moveToNext()) {
			VendaDTO venda = carregarVenda(cursor);
			retorno.add(venda);
		}
		
		return retorno;
	}
	
	public List<VendaDTO> listarVendasPorPeriodo(Date dataInicial, Date dataFinal) {
		List<VendaDTO> retorno = new ArrayList<VendaDTO>();
		List<VendaDTO> listaRemover = new ArrayList<VendaDTO>();
		
		try {
			retorno = listarVendas();
			for (VendaDTO vendaDTO : retorno) {
				if (vendaDTO.getPrevisaoEntrega().compareTo(dateFormatter.parse(dateFormatter.format(dataInicial))) < 0 || vendaDTO.getPrevisaoEntrega().compareTo(dateFormatter.parse(dateFormatter.format(dataFinal))) > 0) {
					listaRemover.add(vendaDTO);
				}
			}
			retorno.removeAll(listaRemover);
		} catch (Exception e) {

		}
		
		return retorno;
	}
	
	public void manter(VendaDTO venda) {
		long idVenda = 0;
		boolean tudoOk = false;
		
		db.beginTransaction();

		try {
			ContentValues contentValuesVenda = new ContentValues();
			
			if (venda.getCodigo() > 0)
				contentValuesVenda.put("_id", venda.getCodigo());
			contentValuesVenda.put("id_cliente", venda.getCliente().getCodigo());
			contentValuesVenda.put("previsao_entrega", dateFormatter.format(venda.getPrevisaoEntrega()));

			idVenda = db.replace(BD.TBL_VENDA, null, contentValuesVenda);
			if (idVenda > 0) {
				for (VendaItemDTO item : venda.getListaItens()) {
					ContentValues contentValuesItem = new ContentValues();
					contentValuesItem.put("id_venda", idVenda);
					contentValuesItem.put("id_produto", item.getProduto().getId());
					contentValuesItem.put("quantidade", item.getQuantidade());
					contentValuesItem.put("valor_unitario", item.getValorUnitario());

					db.replace(BD.TBL_VENDA_ITEM, null, contentValuesItem);
				}
				tudoOk = true;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		
		if (tudoOk) {
			for (VendaItemDTO item : venda.getListaItens()) {
				ProdutoDAO.getInstance(context).baixarEstoque(item.getProduto(), item.getQuantidade());
			}
		}
	}
	
	public List<ComissaoDTO> calcularComissao(Date dataInicial, Date dataFinal) {
		List<ComissaoDTO> lista = new ArrayList<ComissaoDTO>();
		
		List<VendaDTO> vendas = listarVendasPorPeriodo(dataInicial, dataFinal);
		

		SharedPreferences preferences = context.getSharedPreferences("Projeto", Context.MODE_PRIVATE);
		double percComissao = preferences.getFloat("comissao", 0);
		
		Map<Date, Double> comissoes = new HashMap<Date, Double>();
		for (VendaDTO vendaDTO : vendas) {
			double valor = 0;
			if (comissoes.containsKey(vendaDTO.getPrevisaoEntrega())) {
				valor = comissoes.get(vendaDTO.getPrevisaoEntrega()).doubleValue();
			}
			valor += ((vendaDTO.getValorTotalVenda() / 100) * percComissao);
			comissoes.put(vendaDTO.getPrevisaoEntrega(), valor);
		}
		
		for (Date key : comissoes.keySet()) {
			ComissaoDTO comissao = new ComissaoDTO();
			comissao.setData(key);
			comissao.setValorTotal(comissoes.get(key));
			lista.add(comissao);
		}
		
		Collections.sort(lista, new Comparator<ComissaoDTO>() {

			@Override
			public int compare(ComissaoDTO lhs, ComissaoDTO rhs) {

				return lhs.getData().compareTo(rhs.getData());
			}
		});
		
		return lista;
	}
	
	private VendaDTO carregarVenda(Cursor c) {
		VendaDTO venda = new VendaDTO();
		
		int codigo = c.getInt(c.getColumnIndex("_id"));
		int codigoCliente = c.getInt(c.getColumnIndex("id_cliente"));
		Date previsaoEntrega = null;
		
		try {
			previsaoEntrega = dateFormatter.parse(c.getString(c.getColumnIndex("previsao_entrega")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ClienteDTO cliente = ClienteDAO.getInstance(context).buscarCientePorCodigo(codigoCliente);
		
		venda.setCodigo(codigo);
		venda.setCliente(cliente);
		venda.setPrevisaoEntrega(previsaoEntrega);

		Cursor cursorItem = db.query(BD.TBL_VENDA_ITEM, null, BD.VENDA_ITEM_VENDA + " = " + String.valueOf(codigo), null, null, null, null);
		while (cursorItem.moveToNext()) {
			int idProduto = cursorItem.getInt(cursorItem.getColumnIndex("id_produto"));
			ProdutoDTO produto = ProdutoDAO.getInstance(context).buscarProdutoPorId(idProduto);
			
			VendaItemDTO item = new VendaItemDTO(venda, produto);
			item.setQuantidade(cursorItem.getInt(cursorItem.getColumnIndex("quantidade")));
			item.setValorUnitario(cursorItem.getDouble(cursorItem.getColumnIndex("valor_unitario")));
			venda.adicionaItem(item);
		}
		
		
		return venda;
	}

}
