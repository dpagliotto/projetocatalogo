package br.com.toledo.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.toledo.dto.ClienteDTO;

public class ClienteDAO extends BaseDAO {
	
	public SQLiteDatabase db;
	public static ClienteDAO instance = new ClienteDAO();

	public static ClienteDAO getInstance(Context contexto) {
		if (instance.db == null || instance.db.isOpen()) {
			instance.db = new BD(contexto).getWritableDatabase();
		}

		return instance;
	}
	
	public long manter(ClienteDTO cliente) {
		long id = 0;

		db.beginTransaction();

		try {
			ContentValues contentValues = new ContentValues();
			if (cliente.getCodigo() != null && cliente.getCodigo() > 0)
				contentValues.put("_id", cliente.getCodigo());
			contentValues.put("nome", cliente.getNome());
			contentValues.put("cpf", cliente.getCpf());
			contentValues.put("endereco", cliente.getEndereco());
			contentValues.put("telefone", cliente.getTelefone());

			id = db.replace(BD.TBL_CLIENTE, null, contentValues);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return id;
	}
	
	public int excluir(ClienteDTO cliente) {
		int linhasExcluidas = 0;
		db.beginTransaction();
		try {
			linhasExcluidas = db.delete(BD.TBL_CLIENTE, BD.CLIENTE_CODIGO +" = " + String.valueOf(cliente.getCodigo()), null);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		
		return linhasExcluidas;
	}

	public List<ClienteDTO> listar() {
		List<ClienteDTO> lista = new ArrayList<ClienteDTO>();

		Cursor cursor = db.query(BD.TBL_CLIENTE, null, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ClienteDTO cliente = carregarCliente(cursor);
			lista.add(cliente);
			cursor.moveToNext();
		}

		return lista;
	}

	public ClienteDTO buscarCientePorCodigo(int codigo) {
		ClienteDTO cliente = null;

		Cursor cursor = db.query(BD.TBL_CLIENTE, null, BD.CLIENTE_CODIGO + " = " + String.valueOf(codigo), null, null, null, null);
		if (cursor.moveToNext()) {
			cliente = carregarCliente(cursor);
		}

		return cliente;
	}
	
	private ClienteDTO carregarCliente(Cursor c) {
		int codigo = c.getInt(c.getColumnIndex("_id"));
		String nome = c.getString(c.getColumnIndex("nome"));
		String cpf = c.getString(c.getColumnIndex("cpf"));
		String endereco = c.getString(c.getColumnIndex("endereco"));
		String telefone = c.getString(c.getColumnIndex("telefone"));

		ClienteDTO cliente = new ClienteDTO(codigo, nome, cpf, endereco, telefone);
		
		return cliente;
	}

}
