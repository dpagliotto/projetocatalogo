package br.com.toledo.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "projeto.db";
	private static final int DATABASE_VERSION = 9;


	public static final String TBL_PRODUTO = "produto";
	public static final String PRODUTO_ID = "_id";
	public static final String PRODUTO_DESCRICAO = "descricao";
	public static final String PRODUTO_VALOR = "valor";
	public static final String PRODUTO_SALDO_ESTOQUE = "saldo_estoque";
	public static final String PRODUTO_IMAGEM = "imagem";
	public static final String PRODUTO_SITUACAO = "situacao";
	
	public static final String TBL_CLIENTE = "cliente";
	public static final String CLIENTE_CODIGO = "_id";
	public static final String CLIENTE_NOME = "nome";
	public static final String CLIENTE_CPF = "cpf";
	public static final String CLIENTE_TELEFONE = "telefone";
	public static final String CLIENTE_ENDERECO = "endereco";
	
	public static final String TBL_VENDA = "venda";
	public static final String VENDA_ID = "_id";
	public static final String VENDA_CLIENTE = "id_cliente";
	public static final String VENDA_PREVISAO_ENTREGA = "previsao_entrega";
	
	public static final String TBL_VENDA_ITEM = "venda_item";
	public static final String VENDA_ITEM_VENDA = "id_venda";
	public static final String VENDA_ITEM_PRODUTO = "id_produto";
	public static final String VENDA_ITEM_QUANTIDADE = "quantidade";
	public static final String VENDA_ITEM_VALOR_UNITARIO = "valor_unitario";

	public static final String CREATE_PRODUTO= "create table if not exists "
			+ TBL_PRODUTO
			+ "( "
			+ PRODUTO_ID
			+ " integer primary key, "
			+ PRODUTO_DESCRICAO
			+ " text not null, "
			+ PRODUTO_VALOR
			+ " numeric not null, "
			+ PRODUTO_SALDO_ESTOQUE
			+ " numeric not null, "
			+ PRODUTO_IMAGEM
			+ " text not null, "
			+ PRODUTO_SITUACAO
			+ " text not null )" ;

	public static final String CREATE_CLIENTE= "create table if not exists "
			+ TBL_CLIENTE
			+ "( "
			+ CLIENTE_CODIGO
			+ " integer primary key autoincrement, "
			+ CLIENTE_NOME
			+ " text not null, "
			+ CLIENTE_CPF
			+ " text not null,"
			+ CLIENTE_TELEFONE
			+ " text not null,"
			+ CLIENTE_ENDERECO
			+ " text not null )";

	public static final String CREATE_VENDA= "create table if not exists "
			+ TBL_VENDA
			+ "( "
			+ VENDA_ID
			+ " integer primary key autoincrement, "
			+ VENDA_CLIENTE
			+ " integer not null, "
			+ VENDA_PREVISAO_ENTREGA
			+ " text not null) ";

	public static final String CREATE_VENDA_ITEM= "create table if not exists "
			+ TBL_VENDA_ITEM
			+ "( "
			+ VENDA_ITEM_VENDA  
			+ " integer not null, "
			+ VENDA_ITEM_PRODUTO
			+ " integer not null, "
			+ VENDA_ITEM_QUANTIDADE
			+ " integer not null, "
			+ VENDA_ITEM_VALOR_UNITARIO
			+ " numeric not null ) ";

	public BD(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_PRODUTO);
		database.execSQL(CREATE_CLIENTE);
		database.execSQL(CREATE_VENDA);
		database.execSQL(CREATE_VENDA_ITEM);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TBL_PRODUTO);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_CLIENTE);
		onCreate(db);
	}
}