package br.com.toledo.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.toledo.dto.ProdutoDTO;

public class ProdutoDAO {
	
	public SQLiteDatabase db;
	public static ProdutoDAO instance = new ProdutoDAO();

	public static ProdutoDAO getInstance(Context contexto) {
		if (instance.db == null || instance.db.isOpen()) {
			instance.db = new BD(contexto).getWritableDatabase();
		}

		return instance;
	}
	
	public void manterLista(List<ProdutoDTO> produtos) {
		for (ProdutoDTO produtoDTO : produtos) {
			manter(produtoDTO);
		}
	}

	public long manter(ProdutoDTO produto) {
		long id = 0;

		db.beginTransaction();

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put("_id", produto.getId());
			contentValues.put("descricao", produto.getDescricao());
			contentValues.put("valor", produto.getValor());
			contentValues.put("saldo_estoque", produto.getSaldoEstoque());
			contentValues.put("imagem", produto.getImage());
			contentValues.put("situacao", produto.getSituacao());

			db.replace(BD.TBL_PRODUTO, null, contentValues);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}

		return id;
	}
	
	public int excluir(ProdutoDTO produto) {
		int linhasExcluidas = 0;
		db.beginTransaction();
		try {
			linhasExcluidas = db.delete(BD.TBL_PRODUTO, BD.PRODUTO_ID + " = " + String.valueOf(produto.getId()), null);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
		
		return linhasExcluidas;
	}
	
	public List<ProdutoDTO> listarProdutosComEstoque() {
		List<ProdutoDTO> lista = new ArrayList<ProdutoDTO>();

		Cursor cursor = db.query(BD.TBL_PRODUTO, null, BD.PRODUTO_SALDO_ESTOQUE + " > 0 ", null, null, null, null);
		while (cursor.moveToNext()) {
			ProdutoDTO produto = carregarProduto(cursor);
			lista.add(produto);
		}
		cursor.close();

		return lista;
	}

	public List<ProdutoDTO> listarProdutos() {
		List<ProdutoDTO> lista = new ArrayList<ProdutoDTO>();

		Cursor cursor = db.query(BD.TBL_PRODUTO, null, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ProdutoDTO produto = carregarProduto(cursor);
			lista.add(produto);
			cursor.moveToNext();
		}
		cursor.close();

		return lista;
	}
	
	public ProdutoDTO buscarProdutoPorId(int id) {
		ProdutoDTO produto = null;
		
		Cursor cursor = db.query(BD.TBL_PRODUTO, null, BD.PRODUTO_ID + " = " + String.valueOf(id), null, null, null, null);
		if (cursor.moveToNext()) {
			produto = carregarProduto(cursor);
		}
		cursor.close();
		
		return produto;
	}
	
	public void baixarEstoque(ProdutoDTO produto, int quantidadeBaixar) {
		int saldo = produto.getSaldoEstoque();
		produto.setSaldoEstoque(saldo - quantidadeBaixar);
		manter(produto);
	}
	
	
	private ProdutoDTO carregarProduto(Cursor c) {
		int codigo = c.getInt(c.getColumnIndex("_id"));
		String descricao = c.getString(c.getColumnIndex("descricao"));
		Double valor = c.getDouble(c.getColumnIndex("valor"));
		Integer saldoEstoque = c.getInt(c.getColumnIndex("saldo_estoque"));
		String imagem = c.getString(c.getColumnIndex("imagem"));
		String situacao = c.getString(c.getColumnIndex("situacao"));

		ProdutoDTO produto = new ProdutoDTO(codigo, descricao, valor, saldoEstoque, imagem, situacao);
		
		return produto;
	}

}
