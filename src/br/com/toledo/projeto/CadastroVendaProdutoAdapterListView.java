package br.com.toledo.projeto;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.toledo.conexao.ConexaoUtil;
import br.com.toledo.dto.VendaItemDTO;

public class CadastroVendaProdutoAdapterListView extends BaseAdapter {
	
	private Context mContext;
	private List<VendaItemDTO> mListaItem;
	
	private DecimalFormat formmater = new DecimalFormat("###,##0.00");
	private boolean apenasVisualizacao;
	
	public CadastroVendaProdutoAdapterListView(Context context, final List<VendaItemDTO> itens, boolean apenasVisualizacao) {
		super();
		this.mContext = context;
		this.mListaItem = itens;
		this.apenasVisualizacao = apenasVisualizacao;
	}

	public int getCount() {
		return mListaItem.size();
	}

	public VendaItemDTO getItem(int posicao) {
		return mListaItem.get(posicao);
	}

	public long getItemId(int posicao) {
		return posicao;
	}

	public View getView(int posicao, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
		   view = LayoutInflater.from(mContext).inflate(R.layout.venda_item_row_adapter, null);
		}

		final VendaItemDTO item = getItem(posicao);
		
		final String filePath = ConexaoUtil.PATH_IMAGE + String.valueOf(item.getProduto().getId() + ".jpg");
		Bitmap bmp = BitmapFactory.decodeFile(filePath);
		
		ImageView imgProduto = (ImageView) view.findViewById(R.id.ivVendaItemImagem);
		if (bmp != null) {
			imgProduto.setImageBitmap(bmp);
			imgProduto.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					File file = new File(filePath);
					Uri path = Uri.fromFile(file);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.setDataAndType(path, "image/*");
					try {
						mContext.startActivity(intent);
					} catch (ActivityNotFoundException e) {

					}					
				}
			});
		} else {
			imgProduto.setImageResource(R.drawable.sem_foto);
		}
		
		TextView tvProduto = (TextView) view.findViewById(R.id.tvVendaItemProduto);
		tvProduto.setText(item.getProduto().getDescricao());
		
		TextView tvValorUnitario = (TextView) view.findViewById(R.id.tvVendaItemValorUnitario);
		tvValorUnitario.setText("Valor Unit�rio: R$ " + formmater.format(item.getValorUnitario()));
		
		TextView tvSaldoEstoque = (TextView) view.findViewById(R.id.tvVendaItemSaldoEstoque);
		tvSaldoEstoque.setText("Saldo Estoque: " + String.valueOf(item.getProduto().getSaldoEstoque()));
		
		final TextView tvQuantidade = (TextView) view.findViewById(R.id.tvVendaItemQuantidade);
		tvQuantidade.setText(String.valueOf(item.getQuantidade()));
		
		final TextView tvValorTotal = (TextView) view.findViewById(R.id.tvVendaItemValorTotal); 
		tvValorTotal.setText("R$ " + formmater.format(item.getValorTotal()));
		
		ImageButton btnItemAdd = (ImageButton) view.findViewById(R.id.ivVendaItemAdd);
		if (!apenasVisualizacao) {
			btnItemAdd.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (item.getProduto().getSaldoEstoque() > item.getQuantidade()) {
						item.setQuantidade(item.getQuantidade() + 1);
					}

					tvQuantidade.setText(String.valueOf(item.getQuantidade())); 
					tvValorTotal.setText("R$ " + formmater.format(item.getValorTotal()));
					atualizaValorTotal();
				}
			});
		}
		
		ImageButton btnItemDelete = (ImageButton) view.findViewById(R.id.ivVendaItemDelete);
		if (!apenasVisualizacao) {
			btnItemDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (item.getQuantidade() > 0) {
						item.setQuantidade(item.getQuantidade() - 1);
					}

					tvQuantidade.setText(String.valueOf(item.getQuantidade())); 
					tvValorTotal.setText("R$ " + formmater.format(item.getValorTotal()));
					atualizaValorTotal();
				}
			});
		}
		
		return view;
	}
	
	private void atualizaValorTotal() {
		((CadastroVenda) mContext).atualizaValorTotal();
	}

}
