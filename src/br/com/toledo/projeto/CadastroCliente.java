package br.com.toledo.projeto;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.toledo.dao.ClienteDAO;
import br.com.toledo.dto.ClienteDTO;


public class CadastroCliente extends Activity implements View.OnClickListener {

	private ClienteDTO mClienteDTO;
	private ClienteDAO mClienteDAO;
	
	private EditText mEtNome;
	private EditText mEtCpf;
	private EditText mEtEndereco;
	private EditText mEtTelefone;

	private Button mBtCadastrar;
	private Button mBtVoltar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_cliente);
		
		if (getIntent().getExtras() != null)
			mClienteDTO = (ClienteDTO) getIntent().getExtras().getSerializable("cliente");
		
		mClienteDAO = ClienteDAO.getInstance(this);
		
		mEtNome = (EditText) findViewById(R.id.etNome);
		mEtCpf = (EditText) findViewById(R.id.etCpf);
		mEtEndereco = (EditText) findViewById(R.id.etEndereco);
		mEtTelefone = (EditText) findViewById(R.id.etTelefone);
		
		if (mClienteDTO != null) {
			mEtNome.setText(mClienteDTO.getNome());
			mEtCpf.setText(mClienteDTO.getCpf());
			mEtEndereco.setText(mClienteDTO.getEndereco());
			mEtTelefone.setText(mClienteDTO.getTelefone());
		}

		mBtCadastrar = (Button) findViewById(R.id.btnGravarCliente);
		mBtCadastrar.setOnClickListener(this);
		
		mBtVoltar = (Button) findViewById(R.id.btnVoltarCadastroCliente);
		mBtVoltar.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		if (v == mBtCadastrar) {
			if (validaCliente()) {
				cadastrarCliente();
			}
		} else if (v == mBtVoltar) {
			voltarCadastroCliente();
		}
	}
	
	private boolean validaCliente() {
		boolean retorno = true;
		if (mEtNome.getText().toString().isEmpty() || mEtCpf.getText().toString().isEmpty()) {
			retorno = false;

			Toast.makeText(this, "Informe os campos obrigat�rios", Toast.LENGTH_SHORT).show();
		} 

		return retorno;
	}

	private void cadastrarCliente() {
			
		if (mClienteDTO == null) 
			mClienteDTO = new ClienteDTO(null, mEtNome.getText().toString(), mEtCpf.getText().toString(), mEtEndereco.getText().toString(), mEtTelefone.getText().toString());
		else {
			
			mClienteDTO.setNome(mEtNome.getText().toString());
			mClienteDTO.setCpf(mEtCpf.getText().toString());
			mClienteDTO.setEndereco(mEtEndereco.getText().toString());
			mClienteDTO.setTelefone(mEtTelefone.getText().toString());

		}
		
		
		mClienteDAO.manter(mClienteDTO);
		Toast.makeText(this, "Incluido com sucesso", Toast.LENGTH_SHORT).show();
		finish();
	}
	
	private void voltarCadastroCliente() {
		new AlertDialog.Builder(this)
		.setTitle("Confirma��o")
		.setMessage("Deseja cancelar o cadastro?")
		.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		})
		.setNegativeButton("N�o", null)
		.show();
	}

}
