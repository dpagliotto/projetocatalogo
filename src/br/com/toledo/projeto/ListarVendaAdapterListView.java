package br.com.toledo.projeto;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.toledo.dto.VendaDTO;

public class ListarVendaAdapterListView extends BaseAdapter {
	
	private Context mContext;
	private List<VendaDTO> mListaVenda;
	
	private DecimalFormat numberFormatter = new DecimalFormat("###,##0.00");
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
	
	public ListarVendaAdapterListView(Context context, final List<VendaDTO> vendas) {
		super();
		this.mContext = context;
		this.mListaVenda = vendas;
	}

	public int getCount() {
		return mListaVenda.size();
	}

	public VendaDTO getItem(int posicao) {
		return mListaVenda.get(posicao);
	}

	public long getItemId(int posicao) {
		return posicao;
	}

	public View getView(int posicao, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
		   view = LayoutInflater.from(mContext).inflate(R.layout.venda_row_adapter, null);
		}

		VendaDTO venda = getItem(posicao);
		
		TextView txtVenda = (TextView) view.findViewById(R.id.tvVendaNumero);
		txtVenda.setText(String.valueOf(venda.getCodigo()));
		
		TextView txtPrevisaoEntrega = (TextView) view.findViewById(R.id.tvVendaEntrega);
		txtPrevisaoEntrega.setText(dateFormatter.format(venda.getPrevisaoEntrega()));
		
		TextView txtCliente = (TextView) view.findViewById(R.id.tvVendaCliente);
		txtCliente.setText(String.valueOf(venda.getCliente().getNome()));
		
		TextView txtValorTotal = (TextView) view.findViewById(R.id.tvVendaTotal);
		txtValorTotal.setText("R$ " + numberFormatter.format(venda.getValorTotalVenda()));
		
		return view;
	}

}
