package br.com.toledo.projeto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import br.com.toledo.dao.VendaDAO;
import br.com.toledo.dto.ComissaoDTO;

public class Comissao extends Activity implements View.OnClickListener {
	
	private EditText mEtDataInicial;
	
	private EditText mEtDataFinal;
	
	private Button mBtnBuscar;
	
	private ListView mListaComissao;
	
	private VendaDAO mVendaDAO;
	
	private DatePickerDialog mDatePickerDialog;
	
	private SimpleDateFormat mDateFormatter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comissao);
		
		mEtDataInicial = (EditText) findViewById(R.id.etComissaoDataInicial);
		mEtDataInicial.setOnClickListener(this);
		
		mEtDataFinal = (EditText) findViewById(R.id.etComissaoDataFinal);
		mEtDataFinal.setOnClickListener(this);
		criaDatePickerDialog();
		
		mBtnBuscar = (Button) findViewById(R.id.btnComissaoBuscar);
		mBtnBuscar.setOnClickListener(this);
		
		mListaComissao = (ListView) findViewById(R.id.lvComissao);
		
		mVendaDAO = VendaDAO.getInstance(this);
	}
	
	private void criaDatePickerDialog() {
		mDateFormatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt","BR"));
		
		final Calendar newCalendar = Calendar.getInstance();
		
		mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				((EditText) view.getTag()).setText(mDateFormatter.format(newDate.getTime()));
				((EditText) view.getTag()).setTag(newDate.getTime());
			}		
		}, 
		newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.etComissaoDataInicial || v.getId() == R.id.etComissaoDataFinal) {
			mDatePickerDialog.getDatePicker().setTag(v);
			mDatePickerDialog.show();
		} else if (v.getId() == R.id.btnComissaoBuscar) {
			
			List<ComissaoDTO> listaComissao = new ArrayList<ComissaoDTO>();
			if (mEtDataInicial.getTag() != null && mEtDataFinal.getTag() != null) {
				Date dataInicial = (Date) mEtDataInicial.getTag();
				Date dataFinal = (Date) mEtDataFinal.getTag();

				listaComissao = mVendaDAO.calcularComissao(dataInicial, dataFinal);
				if (listaComissao.size() > 0) {
				} else {
					Toast.makeText(this, "N�o h� vendas no periodo selecionado!", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(this, "Informe o per�odo desejado!", Toast.LENGTH_SHORT).show();
			}
			
			ComissaoAdapterListView adapter = new ComissaoAdapterListView(this, listaComissao);
			mListaComissao.setAdapter(adapter);
		}
	}
}
