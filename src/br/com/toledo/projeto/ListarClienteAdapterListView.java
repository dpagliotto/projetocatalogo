package br.com.toledo.projeto;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.toledo.dto.ClienteDTO;

public class ListarClienteAdapterListView extends BaseAdapter {
	
	private Context mContext;
	private List<ClienteDTO> mListaClientes;
	
	public ListarClienteAdapterListView(Context context, List<ClienteDTO> clientes) {
		super();
		this.mContext = context;
		this.mListaClientes = clientes;
	}

	public int getCount() {
		return mListaClientes.size();
	}

	public Object getItem(int posicao) {
		return mListaClientes.get(posicao);
	}

	public long getItemId(int posicao) {
		return posicao;
	}

	public View getView(int posicao, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
		   view = LayoutInflater.from(mContext).inflate(R.layout.cliente_row_adapter, null);
		}

		ClienteDTO cliente = (ClienteDTO) getItem(posicao);
		
		TextView txtNome = (TextView) view.findViewById(R.id.tvNomeCliente);	
		txtNome.setText(cliente.getNome());
		
		TextView txtCPF = (TextView) view.findViewById(R.id.tvCPFCliente);
		txtCPF.setText(cliente.getCpf());
		
		TextView txtEndereco = (TextView) view.findViewById(R.id.tvEnderecoCliente);
		txtEndereco.setText(cliente.getEndereco());
		
		TextView txtTelefone = (TextView) view.findViewById(R.id.tvTelefoneCliente);
		txtTelefone.setText(cliente.getTelefone());

		return view;
	}


}
