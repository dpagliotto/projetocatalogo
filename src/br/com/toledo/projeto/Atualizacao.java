package br.com.toledo.projeto;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import br.com.toledo.conexao.ConexaoProduto;
import br.com.toledo.dao.ProdutoDAO;
import br.com.toledo.dto.ProdutoDTO;

public class Atualizacao extends Activity implements View.OnClickListener {

	private Button mBtAtualizar;
	private ListView mListViewAtualizacao;
	private Button mBtConfirmar;

	private List<ProdutoDTO> mListaProduto;
	private ProdutoDAO mProdutoDAO;

	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_atualizacao);

		mProdutoDAO = ProdutoDAO.getInstance(this);

		mHandler = new Handler();

		mBtAtualizar = (Button) findViewById(R.id.btnAtualizar);
		mBtAtualizar.setOnClickListener(this);

		mListViewAtualizacao = (ListView) findViewById(R.id.lvListaAtualizacao);
		listarProdutos();

		mBtConfirmar = (Button) findViewById(R.id.btnConfirmar);
		mBtConfirmar.setVisibility(View.GONE);
		mBtConfirmar.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnAtualizar) {
			atualizar();
		} else if (v.getId() == R.id.btnConfirmar) {
			confirmar();
		}
	}
	
	private void atualizar() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				ConexaoProduto conexao = new ConexaoProduto(); 
				List<ProdutoDTO> produtosAtualizados =  conexao.buscarTodosProdutos(Atualizacao.this);
				
				if (produtosAtualizados != null) {
					if (produtosAtualizados.size() > 0) {
						listarProdutos();
						mostrarMensagem("Produtos atualizados!");
					} else {
						mostrarMensagem("N�o h� produtos para atualizar");
					}
				} else {
					mostrarMensagem("Erro ao realizar atualiza��o! "
							+ "\nVerifique sua conex�o com a internet");
				}
			}
		}).start();
	}
	
	private void listarProdutos() {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				mListaProduto = mProdutoDAO.listarProdutos();
				AtualizacaoAdapterListView adapter = new AtualizacaoAdapterListView(Atualizacao.this, mListaProduto);
				mListViewAtualizacao.setAdapter(adapter);
				mBtConfirmar.setVisibility(View.VISIBLE);
			}
		});
	}
	
	private void confirmar() {
		new AlertDialog.Builder(this)
		.setMessage("Deseja gravar as informa��es da atualiza��o?")
		.setTitle("Confirma��o")
		.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mProdutoDAO.manterLista(mListaProduto);
				Toast.makeText(Atualizacao.this, "Atualiza��o finalizada com sucesso!", Toast.LENGTH_SHORT).show();
				Atualizacao.this.finish();
			}
		})
		.setNegativeButton("N�o", null)
		.show();
	}

	private void mostrarMensagem(final String mensagem) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(Atualizacao.this, mensagem, Toast.LENGTH_SHORT).show();
			}
		});
	}

}
