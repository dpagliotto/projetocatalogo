package br.com.toledo.projeto;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.toledo.dao.ClienteDAO;
import br.com.toledo.dao.ProdutoDAO;
import br.com.toledo.dao.VendaDAO;
import br.com.toledo.dto.ClienteDTO;
import br.com.toledo.dto.ProdutoDTO;
import br.com.toledo.dto.VendaDTO;
import br.com.toledo.dto.VendaItemDTO;

public class CadastroVenda extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

	private Spinner mSpnCliente;
	private EditText mEtPrevisaoEntrega;
	private ListView mLstItens;
	private TextView mTvValorTotal;
	private Button mBtCadastrar;

	private VendaDTO mVenda;

	private ClienteDAO mClienteDAO;
	private List<ClienteDTO> mListaCliente;

	private SimpleDateFormat mDateFormatter;
	private DatePickerDialog mDatePickerDialog;

	private Boolean apenasVisualizacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_venda);

		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("venda")) {
			mVenda = (VendaDTO) getIntent().getExtras().get("venda");
		} else {
			mVenda = new VendaDTO();
		}

		apenasVisualizacao = true;

		if (mVenda.getQuantidadeItens() == 0) {
			apenasVisualizacao = false;

			List<ProdutoDTO> produtosEstoque = ProdutoDAO.getInstance(this).listarProdutosComEstoque();
			for (ProdutoDTO produtoDTO : produtosEstoque) {
				VendaItemDTO item = new VendaItemDTO(mVenda, produtoDTO);
				item.setQuantidade(0);
				mVenda.adicionaItem(item);
			}
		}
		
		carregarClientes();
		mSpnCliente = (Spinner) findViewById(R.id.spVendaCliente);
		mSpnCliente.setOnItemSelectedListener(this);
		adapterSpnCliente();

		mEtPrevisaoEntrega = (EditText) findViewById(R.id.etVendaEntrega);
		mEtPrevisaoEntrega.setOnClickListener(this);
		criaDatePickerDialog();
		
		mLstItens = (ListView) findViewById(R.id.lstListaItemVenda);
		adapterLstItens();
		
		mTvValorTotal = (TextView) findViewById(R.id.tvVendaValorTotal);
		atualizaValorTotal();

		mBtCadastrar = (Button) findViewById(R.id.btnGravarVenda);
		mBtCadastrar.setOnClickListener(this);
		
		if (apenasVisualizacao) {
			mSpnCliente.setEnabled(false);
			mEtPrevisaoEntrega.setEnabled(false);
			mBtCadastrar.setEnabled(false);
		}
	}

	// View.OnClickListener
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnGravarVenda) {
			cadastrarVenda();
		} else if (v.getId() == R.id.etVendaEntrega) {
			mDatePickerDialog.show();
		}
	}

	// AdapterView.OnItemSelectedListener
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent == mSpnCliente) {
			mVenda.setCliente(mListaCliente.get(position));
		}
	}
	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}	

	private void carregarClientes() {
		mClienteDAO = ClienteDAO.getInstance(this);
		mListaCliente = new ArrayList<ClienteDTO>();
		mListaCliente.add(new ClienteDTO());
		mListaCliente.addAll(mClienteDAO.listar());
	}

	private void adapterSpnCliente() {
		List<String> listaAdapter = new ArrayList<String>();
		int idx = 0;
		int pos = 0;
		for (ClienteDTO cliente : mListaCliente) {
			listaAdapter.add(cliente.getNome());
			if (cliente.getCodigo().equals(mVenda.getCliente().getCodigo())) {
				pos = idx;
			}
			idx++;
		}
		ArrayAdapter<String> adapterClientes = new ArrayAdapter<String>(this, 
				android.R.layout.simple_spinner_item, listaAdapter);
		mSpnCliente.setAdapter(adapterClientes);

		Message msg = new Message();
		msg.getData().putInt("pos", pos);
		new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				int pos = msg.getData().getInt("pos");
				mSpnCliente.setSelection(pos);
				return false;
			}
		}).sendMessage(msg);
	}

	public void atualizaValorTotal() {
		DecimalFormat formatter = new DecimalFormat("###0.00");
		mTvValorTotal.setText("Valor Total: R$" + formatter.format(mVenda.getValorTotalVenda()));
	}
	
	private void criaDatePickerDialog() {
		mDateFormatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt","BR"));
		
		final Calendar newCalendar = Calendar.getInstance();
		if (mVenda.getPrevisaoEntrega() != null)
			newCalendar.setTime(mVenda.getPrevisaoEntrega());
		
		mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				mVenda.setPrevisaoEntrega(newDate.getTime());
				mEtPrevisaoEntrega.setText(mDateFormatter.format(newDate.getTime()));
			}		
		}, 
		newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
		
		if (mVenda.getPrevisaoEntrega() != null)
			mEtPrevisaoEntrega.setText(mDateFormatter.format(mVenda.getPrevisaoEntrega()));
	}

	private void adapterLstItens() {
		CadastroVendaProdutoAdapterListView adapterItens = new CadastroVendaProdutoAdapterListView(this, mVenda.getListaItens(), apenasVisualizacao);
		mLstItens.setAdapter(adapterItens);
	}

	private void cadastrarVenda() {

		if (mVenda.getCliente().getCodigo() == 0) {
			Toast.makeText(this, "N�o foi selecionado nenhum cliente!", Toast.LENGTH_SHORT).show();
			return;
		}

		if (mVenda.getPrevisaoEntrega() == null) {
			Toast.makeText(this, "N�o foi selecionado a previs�o de entrega!", Toast.LENGTH_SHORT).show();
			return;
		}

		if (mVenda.getQuantidadeTotalUnidades() == 0) {
			Toast.makeText(this, "N�o foi adicionado nenhum produto!", Toast.LENGTH_SHORT).show();
			return;
		}

		new AlertDialog.Builder(this)
		.setTitle("Confirma��o")
		.setMessage("Deseja cadastrar a venda?")
		.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				List<VendaItemDTO> listaRemover = new ArrayList<VendaItemDTO>();

				for (int i = 0; i < mLstItens.getCount(); i++) {
					VendaItemDTO item = (VendaItemDTO) mLstItens.getItemAtPosition(i);
					if (item.getQuantidade() == 0) {
						listaRemover.add(item);
					} 
				}

				mVenda.removeVariosItem(listaRemover);
				CadastroVenda.this.finish();
				VendaDAO.getInstance(CadastroVenda.this).manter(mVenda);
			}
		})
		.setNegativeButton("N�o", null)
		.show();
	}

}
