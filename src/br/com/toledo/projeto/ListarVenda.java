package br.com.toledo.projeto;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import br.com.toledo.dao.VendaDAO;
import br.com.toledo.dto.VendaDTO;

public class ListarVenda extends Activity implements View.OnClickListener {

	private ListView mListView;
	private List<VendaDTO> mListaVenda;
	private VendaDAO mVendaDAO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listar_venda);

		mVendaDAO = VendaDAO.getInstance(this);

		vendaListView();

		((Button) findViewById(R.id.btnCadastroVenda)).setOnClickListener(this);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		vendaListView();
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, CadastroVenda.class);
		startActivity(intent);

		vendaListView();
	}

	private void vendaListView() {
		if (mListView == null) {
			mListView = (ListView) findViewById(R.id.lvListarVenda);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					VendaDTO venda = mListaVenda.get(position);

					Intent intent = new Intent(ListarVenda.this, CadastroVenda.class);
					intent.putExtra("venda", venda);
					startActivity(intent);
				}
			});
		}

		mListaVenda = mVendaDAO.listarVendas();

		if (mListaVenda.size() > 0) {
			List<String> listaAdapter = new ArrayList<String>();
			for (VendaDTO venda : mListaVenda) {
				listaAdapter.add(venda.toString());
			}
			
			ListarVendaAdapterListView adapter = new ListarVendaAdapterListView(this, mListaVenda);
			mListView.setAdapter(adapter);
		} else {		
			Toast.makeText(this, "Nenhuma venda encontrada", Toast.LENGTH_SHORT).show();
		}

	}

}