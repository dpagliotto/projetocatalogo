package br.com.toledo.projeto;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.toledo.dto.ComissaoDTO;
import br.com.toledo.dto.VendaDTO;

public class ComissaoAdapterListView extends BaseAdapter {
	
	private Context mContext;
	private List<ComissaoDTO> mListaComissao;
	
	private DecimalFormat numberFormatter = new DecimalFormat("###,##0.00");
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
	
	public ComissaoAdapterListView(Context context, final List<ComissaoDTO> vendas) {
		super();
		this.mContext = context;
		this.mListaComissao = vendas;
	}

	public int getCount() {
		return mListaComissao.size();
	}

	public ComissaoDTO getItem(int posicao) {
		return mListaComissao.get(posicao);
	}

	public long getItemId(int posicao) {
		return posicao;
	}

	public View getView(int posicao, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
		   view = LayoutInflater.from(mContext).inflate(R.layout.comissao_row_adapter, null);
		}

		ComissaoDTO comissao = getItem(posicao);
		
		TextView txtData = (TextView) view.findViewById(R.id.tvComissaoData);
		txtData.setText(dateFormatter.format(comissao.getData()));
		
		TextView txtValorTotal = (TextView) view.findViewById(R.id.tvComissaoValorTotal);
		txtValorTotal.setText("R$ " + numberFormatter.format(comissao.getValorTotal()));
		
		return view;
	}

}
