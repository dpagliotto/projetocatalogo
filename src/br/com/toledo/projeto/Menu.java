package br.com.toledo.projeto;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.com.toledo.dao.BD;
import br.com.toledo.dao.BaseDAO;

public class Menu extends Activity implements OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		SQLiteDatabase dataBase = new BD(this).getWritableDatabase();
		BaseDAO.setDataBase(dataBase);

		((Button) findViewById(R.id.btnCliente)).setOnClickListener(this);
		((Button) findViewById(R.id.btnVenda)).setOnClickListener(this);
		((Button) findViewById(R.id.btnRelatorioComissao)).setOnClickListener(this);
		((Button) findViewById(R.id.btnAtualizacao)).setOnClickListener(this);

	}
	
	public void onClick(View v) {
		Intent intent = null;
		if (v.getId() == R.id.btnCliente) {
			intent = new Intent(this, ListarCliente.class);
		} else if (v.getId() == R.id.btnVenda) {
			intent = new Intent(this, ListarVenda.class);
		} else if (v.getId() == R.id.btnRelatorioComissao)	{
			intent = new Intent(this, Comissao.class);
		} else if (v.getId() == R.id.btnAtualizacao) {
			intent = new Intent(this, Atualizacao.class);
		}
		startActivityForResult(intent, 1);
	}
}
