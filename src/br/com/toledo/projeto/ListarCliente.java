package br.com.toledo.projeto;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import br.com.toledo.dao.ClienteDAO;
import br.com.toledo.dto.ClienteDTO;

public class ListarCliente extends Activity implements View.OnClickListener {

	private ListView mListView;
	private List<ClienteDTO> mListaCliente;
	private ClienteDAO mClienteDAO;
	
	private Boolean isLongClick = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listar_cliente);
		
		mClienteDAO = ClienteDAO.getInstance(this);
		
		clienteListView();

		((Button) findViewById(R.id.btnCadastroCliente)).setOnClickListener(this);
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		clienteListView();
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, CadastroCliente.class);
		startActivity(intent);
		
		clienteListView();
	}
	
	private void clienteListView() {
		if (mListView == null) {
			mListView = (ListView) findViewById(R.id.lvListarCliente);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (!isLongClick) {
						ClienteDTO cliente = (ClienteDTO) parent.getItemAtPosition(position);

						Intent intent = new Intent(ListarCliente.this, CadastroCliente.class);
						intent.putExtra("cliente", cliente);
						startActivity(intent);
					}
				}
			});
			//Excluir Cliente 
		/*	mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					isLongClick = true;
					final ClienteDTO cliente = (ClienteDTO) parent.getItemAtPosition(position);
					new AlertDialog.Builder(ListarCliente.this)
					.setTitle("Confirma��o")
					.setMessage("Deseja exlcuir o cliente?")
					.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mClienteDAO.excluir(cliente);
							clienteListView();
							isLongClick = false;
						}
					})
					.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							isLongClick = false;
						}
					})
					.setCancelable(false)
					.show();
					return false;
				}
			});*/
		}
		//Listar Cliente n�o ta rolando #Henrique&Juliano
		mListaCliente = mClienteDAO.listar();

		if (mListaCliente.size() > 0) {
			ListarClienteAdapterListView adapterListView = new ListarClienteAdapterListView(this,  mListaCliente);
			mListView.setAdapter(adapterListView);
			
		} else {		
			Toast.makeText(this, "Nenhum cliente encontrado", Toast.LENGTH_SHORT).show();
		}
		
	}

}