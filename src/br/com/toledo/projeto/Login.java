package br.com.toledo.projeto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.toledo.conexao.ConexaoLogin;
import br.com.toledo.dto.UsuarioDTO;

public class Login extends Activity implements OnClickListener {
	
	private EditText mEdtNome;
	private EditText mEdtSenha;
	private TextView mTxtMensagem;
	private Button mBtnAcessar;
	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mHandler = new Handler();
	
		mEdtNome = (EditText) findViewById(R.id.etLogin);
		mEdtSenha = (EditText) findViewById(R.id.etSenha);
		mTxtMensagem = (TextView) findViewById(R.id.tvMensagem);
		mTxtMensagem.setVisibility(View.GONE);
		
		mBtnAcessar = (Button) findViewById(R.id.btnAcessar);
		mBtnAcessar.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		logar();
	}

	private void logar() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String nome = mEdtNome.getText().toString();
				String senha = mEdtSenha.getText().toString();
				
				if ("".equals(nome) || "".equals(senha)) {
					mostrarMensagemErro("Informe o usu�rio e senha!");
					return;
				}

				UsuarioDTO usuario = (new ConexaoLogin()).
						buscarUsuarioPorNomeSenha(Login.this, nome, senha);

				if (usuario == null) {
					String mensagem = "Erro ao realizar login";
					mostrarMensagemErro(mensagem);
				} else {
					if (nome.equalsIgnoreCase(usuario.getNome()) && senha.equalsIgnoreCase(usuario.getSenha()))
						chamarTelaMenu();
					else {
						String mensagem = "Usu�rio e/ou senha inv�lidos";
						mostrarMensagemErro(mensagem);
					}
				}
			}
		}).start();
		
	}

	private void mostrarMensagemErro(final String mensagem) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
        		mTxtMensagem.setText(mensagem);
        		mTxtMensagem.setVisibility(View.VISIBLE);
            }
        });
	}

	private void chamarTelaMenu() {
		Intent intentMenu = new Intent(Login.this, Menu.class);
		startActivity(intentMenu);
		
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				Login.this.finish();
			}
		});
	}	
	
}