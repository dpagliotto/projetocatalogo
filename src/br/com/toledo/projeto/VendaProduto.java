package br.com.toledo.projeto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class VendaProduto extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_venda_produto);
		
		((Button) findViewById(R.id.btnCompra))
				.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		Intent intentTelaVenda = new Intent(this,
				Venda.class);

		startActivityForResult(intentTelaVenda, 1);

		/* startActivityForResult(intentTelaCadastroCliente, 1); */

	}

}