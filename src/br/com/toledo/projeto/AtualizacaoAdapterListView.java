package br.com.toledo.projeto;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.toledo.conexao.ConexaoUtil;
import br.com.toledo.dto.ProdutoDTO;

public class AtualizacaoAdapterListView extends BaseAdapter {
	
	private Context mContext;
	private List<ProdutoDTO> mListaProdutos;
	
	private DecimalFormat formmater = new DecimalFormat("###,##0.00");
	
	public AtualizacaoAdapterListView(Context context, final List<ProdutoDTO> produtos) {
		super();
		this.mContext = context;
		this.mListaProdutos = produtos;
	}

	public int getCount() {
		return mListaProdutos.size();
	}

	public ProdutoDTO getItem(int posicao) {
		return mListaProdutos.get(posicao);
	}

	public long getItemId(int posicao) {
		return posicao;
	}

	public View getView(int posicao, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
		   view = LayoutInflater.from(mContext).inflate(R.layout.produto_row_adapter, null);
		}

		final ProdutoDTO produto = getItem(posicao);
		
		final String filePath = ConexaoUtil.PATH_IMAGE + String.valueOf(produto.getId() + ".jpg");
		Bitmap bmp = BitmapFactory.decodeFile(filePath);
		
		ImageView imgProduto = (ImageView) view.findViewById(R.id.ivProdutoImagem);
		if (bmp != null) {
			imgProduto.setImageBitmap(bmp);
			imgProduto.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					File file = new File(filePath);
					Uri path = Uri.fromFile(file);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.setDataAndType(path, "image/*");
					try {
						mContext.startActivity(intent);
					} catch (ActivityNotFoundException e) {

					}					
				}
			});
		} else {
			imgProduto.setImageResource(R.drawable.sem_foto);
		}
		
		TextView txtDescricao = (TextView) view.findViewById(R.id.tvProdutoDescricao);
		txtDescricao.setText(produto.getDescricao());
		
		TextView txtValor = (TextView) view.findViewById(R.id.tvProdutoValor);
		txtValor.setText("R$ " + formmater.format(produto.getValor()));
		
		TextView txtSituacao = (TextView) view.findViewById(R.id.tvProdutoSituacao);
		txtSituacao.setText("A".equals(produto.getSituacao()) ? "Ativo" : "Inativo");
		
		final TextView txtSaldo = (TextView) view.findViewById(R.id.tvProdutoSaldoEstoque);
		txtSaldo.setText(String.valueOf(produto.getSaldoEstoque()));

		ImageButton btnAdd = (ImageButton) view.findViewById(R.id.ivProdutoAdd);
		btnAdd.setVisibility(View.GONE);
		if ("A".equals(produto.getSituacao())) {
			btnAdd.setVisibility(View.VISIBLE);
			btnAdd.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					produto.setSaldoEstoque(produto.getSaldoEstoque()+1);
					txtSaldo.setText(String.valueOf(produto.getSaldoEstoque()));
				}
			});
		}
		
		ImageButton btnDelete = (ImageButton) view.findViewById(R.id.ivProdutoDelete);
		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (produto.getSaldoEstoque() > 0) {
					produto.setSaldoEstoque(produto.getSaldoEstoque()-1);
					txtSaldo.setText(String.valueOf(produto.getSaldoEstoque()));
				}
			}
		});

		return view;
	}

}
