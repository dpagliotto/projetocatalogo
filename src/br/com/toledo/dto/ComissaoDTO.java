package br.com.toledo.dto;

import java.util.Date;

public class ComissaoDTO {
	
	private Date data;
	
	private Double valorTotal;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

}
