package br.com.toledo.dto;

import java.io.Serializable;

public class ProdutoDTO implements Serializable {

	private static final long serialVersionUID = 7344361017601750392L;

	private int id;

	private String descricao;

	private double valor;

	private int saldoEstoque;

	private String image;
	
	private String situacao;

	public ProdutoDTO() {

	}

	public ProdutoDTO(int id, String descricao, double valor, int saldoEstoque, String image, String situacao) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
		this.saldoEstoque = saldoEstoque;
		this.image = image;
		this.situacao = situacao;
	}

	public ProdutoDTO(int id, String descricao, double valor, String image, String situacao) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
		this.image = image;
		this.situacao = situacao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getSaldoEstoque() {
		return saldoEstoque;
	}

	public void setSaldoEstoque(int saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

}
