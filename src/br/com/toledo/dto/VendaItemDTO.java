package br.com.toledo.dto;

import java.io.Serializable;

public class VendaItemDTO implements Serializable {


	private static final long serialVersionUID = 928474440928483042L;

	private VendaDTO venda;

	private ProdutoDTO produto;
	
	private int quantidade;
	
	private double valorUnitario;

	public VendaItemDTO() {

	}

	public VendaItemDTO(VendaDTO venda) {
		this.venda = venda;
		this.produto = new ProdutoDTO();
	}

	public VendaItemDTO(VendaDTO venda, ProdutoDTO produto) {
		this.venda = venda;
		this.produto = produto;
		this.valorUnitario = this.produto.getValor();
	}

	public VendaDTO getVenda() {
		return venda;
	}

	public void setVenda(VendaDTO venda) {
		this.venda = venda;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	
	public double getValorTotal() {
		return (this.valorUnitario * this.quantidade);
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
