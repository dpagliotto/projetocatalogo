package br.com.toledo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VendaDTO implements Serializable {

	private static final long serialVersionUID = 2689586064930528149L;

	private int codigo;
	
	private ClienteDTO cliente;
	
	private Date previsaoEntrega;
	
	private List<VendaItemDTO> listaItens;
	
	public VendaDTO() {
		this.cliente = new ClienteDTO();
		this.previsaoEntrega = new Date(System.currentTimeMillis());
		this.listaItens = new ArrayList<VendaItemDTO>();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public Date getPrevisaoEntrega() {
		return previsaoEntrega;
	}

	public void setPrevisaoEntrega(Date previsaoEntrega) {
		this.previsaoEntrega = previsaoEntrega;
	}

	public List<VendaItemDTO> getListaItens() {
		return listaItens;
	}

	public void setListaItens(List<VendaItemDTO> listaItens) {
		this.listaItens = listaItens;
	}
	
	public void adicionaItem(VendaItemDTO item) {
		this.listaItens.add(item);
	}
	
	public void removeItem(VendaItemDTO item) {
		this.listaItens.remove(item);
	}
	
	public void removeVariosItem(List<VendaItemDTO> itens) {
		this.listaItens.removeAll(itens);
	}
	
	public double getValorTotalVenda() {
		double valorTotalVenda = 0;
		for (VendaItemDTO item : listaItens) {
			valorTotalVenda += item.getValorTotal();
		}
		return valorTotalVenda;
	}
	
	public int getQuantidadeTotalUnidades() {
		int qtdeTotalUnidades = 0;
		for (VendaItemDTO item : listaItens) {
			qtdeTotalUnidades += item.getQuantidade();
		}
		return qtdeTotalUnidades;
	}
	
	public int getQuantidadeItens() {
		return listaItens.size();
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Venda Nro: " + this.codigo );
		builder.append("\nPrevis�o de entrega: " + this.previsaoEntrega);
		builder.append("\nCliente: " + this.cliente.getNome());
		builder.append("\nValor Total: " + this.getValorTotalVenda());
		return builder.toString();
	}

}
