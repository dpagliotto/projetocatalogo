package br.com.toledo.dto;

import java.io.Serializable;


public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = -8446964672184600501L;

	private Integer codigo;
	private String nome;
	private String cpf;
	private String endereco;
	private String telefone;
	
	public ClienteDTO() {
		this.codigo = 0;
		this.nome = "";
		this.cpf = "";
		this.endereco = "";
		this.telefone = "";
	}

	public ClienteDTO(Integer codigo, String nome, String cpf, String endereco, String telefone) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
